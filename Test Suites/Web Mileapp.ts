<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Web Mileapp</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ae2b303d-bec4-437e-a125-9b76a83b82c3</testSuiteGuid>
   <testCaseLink>
      <guid>a7bea60e-fb50-448d-91ae-b15c0ecee981</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TCM1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6096458d-ab1d-4d2c-85a7-9189c5bf90d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TCM2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee517e90-ceb0-42ae-b7d4-0cb803be9dc8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TCM3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8f19844-492c-4166-a8a2-b98614c89dee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TCM4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d964f9d-7000-4ec0-b5fb-23c950c3937a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TCM5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a421531-69c6-42b0-8a57-b856a344b867</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TCM6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6023a26-8960-434e-9967-b0ec9b86b860</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TCM7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e70b023-7a60-42d6-9f30-5488d6d69494</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TCM8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab4f836f-ae15-4109-a658-aeda8d5b71aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TCM9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2adc9042-532e-40c5-899a-731d55ad85f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TCM10</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c5cf337-28cd-44dd-8a2c-2904fb7b368b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TCM11</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7d07ef3-6651-4508-aff1-5af042eda092</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TCM12</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>263f0603-b7fa-44d7-b8a8-08f1328022ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TCM13</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9879671f-4801-435b-b645-efb7be637363</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TCM14</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e873caec-f29e-45a3-969f-d46be56cf6dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TCM15</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
